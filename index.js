// Tạo function kiểm tra số nguyên tố

function checkPrimeNumber(number){
  var checkPrimeNumber = true;
  for (var i = 2; i <= Math.sqrt(number); i++){
    if (number % i == 0){
      checkPrimeNumber = false;
      break;
    }
  }
  return checkPrimeNumber;
}

// Function in ra số nguyên

function soNguyen() {
  var inputNumber = document.getElementById('txt-number').value*1;
  var outputStringNumber = '';

  for(var n = 2; n <= inputNumber; n++){
    var checkSNT = checkPrimeNumber(n);
    if(checkSNT){
      outputStringNumber += `${n}` ;
    }
  }
  document.getElementById("result01").innerHTML = outputStringNumber;
}